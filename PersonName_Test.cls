@isTest
public class PersonName_Test {
    @IsTest
    static void getFullName_Test(){
        
        Test.startTest();
        PersonName pName = new PersonName('Alena');
        String actualResult1 = pName.getFullName();
        PersonName pName2 = new PersonName('Alena','Dast');
        String actualResult2 = pName2.getFullName();
        System.assertEquals('Alena unknown',actualResult1,'getFullName:');
        System.assertEquals('Alena Dast',actualResult2,'getFullName:');
        Test.stopTest();
        
    }
}
