@isTest
public class makeCounter_Test {
    @IsTest
    static void makeCounterMethods_Test(){
        
        Test.startTest();
            makeCounter mc = new makeCounter();
            mc.setCount(10);
            mc.increase();
            mc.decrease();
            Integer actualResult = mc.getCount();
            System.assertEquals(10, actualResult, 'makeCounter class');

        Test.stopTest();
        
    }
}
