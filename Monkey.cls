public virtual class Monkey extends Animal {
    public Monkey() {
        super('Monkey');
    }
    public Monkey(String setName){
        super(setName);
    }
    public String roar() {
        return Name+' roar';
    }
    public String climb() {
        return Name+' climb';
    }
}
