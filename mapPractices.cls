public class mapPractices {
    public static Map<String, String> getMapAccounts() {
        Account[] accs = [SELECT id , name FROM Account LIMIT 3];
        Map<String, String> mapResult = new Map<String, String>();
        for (Account el : accs) {
            mapResult.put(el.id,el.name);
        }
        return mapResult;
    }
    public static Map<ID, sObject> convertListToMap(List<Account> accList) {
        Map<ID, sObject> mapResult = new Map<ID, sObject>();
        for (Account accEl : accList) {
            mapResult.put(accEl.id, accEl);
        }
        return mapResult;
    }
    public static void getContactsMapWithAccountNameKey() {
        List<Account> accList = [SELECT Name, (SELECT name FROM Contacts) FROM Account];
        Map<String, List<Contact>> mapAccCont = new Map<String, List<Contact>>();
        for (Account cont : accList) {
           mapAccCont.put(cont.Name, new List<Contact>(cont.Contacts));
        }
       System.debug(mapAccCont);
        
    }
}
