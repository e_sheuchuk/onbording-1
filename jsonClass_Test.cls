@isTest
public class jsonClass_Test {
   @IsTest
   static void json_Test(){
       
       Test.startTest();
      
        String testString = '{"firstName":"Борис", "lastName":"ХренПопадешь"}';
        PersonName pn = JsonClass.getPersonFromString(testString);      
        System.assertEquals('Борис ХренПопадешь', pn.getFullName(), 'JSON: Wrong firstName or lastName!');

       Test.stopTest();
       
   }
}
