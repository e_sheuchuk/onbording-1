public virtual class Animal { 
    protected String Name {get;set;}
    public Animal() {
        Name = 'Animal';
    }
    public Animal(String setName) {
        Name = setName;
    }
    public String walk() {
        String result = Name +' walk';
        System.debug(result);
        return result;   
    }
    public String eat() {
        String result = Name +' eat';
        System.debug(result);
        return result;
    }
    public String sleep() {
        String result = Name +' sleep';
        System.debug(result);
        return result;
    }
    public String getName(){
        return Name;
    }
}
